INTRODUCTION
------------
The gcalviews module integrates Google Calendar with the Views module.

* For a full description of the module, visit the project page:
  https://drupal.org/project/gcalviews
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/gcalviews

REQUIREMENTS
------------
This module requires the following modules:
* Views (https://drupal.org/project/views)
* GCal (https://drupal.org/project/gcal)
* GAuth (https://drupal.org/project/gauth) (required by both gcal and gcalviews)

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.
  
CONFIGURATION
-------------
* Configure user permissions in Administration » People » Permissions:
  - Administer Google Calender API integration with Views
    Grants permission to manage Google Calendars.
* Manage Google Calendars in Administration » Settings »
  Web Services » Google Calendar API integration with Views.
