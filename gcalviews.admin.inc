<?php
/**
 * @file
 * Administration pages for gcalview settings.
 */

/**
 * Page callback for gcalviews settings page.
 */
function gcalviews_overview($form, &$form_state) {

  $form = array();

  $form['add_calendar'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Add a Google Calendar to the Views module'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['add_calendar']['calendar_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Calendar ID'),
    '#description' => t('Add a Google Calendar, typically in the form of <em>your_email@gmail.com</em> or <em>your_calendar_id@group.calendar.google.com</em>'),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form['add_calendar']['account_id'] = array(
    '#type' => 'select',
    '#title' => t('Google Account'),
    '#description' => t('Select a Google Account to retrieve the calendar with. You can manage Google Accounts at !url.', array('!url' => l(t('Google Account Settings'), "admin/config/services/gauth_account"))),
    '#options' => gcalviews_generate_account_list(),
    '#required' => TRUE,
  );

  $form['add_calendar']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Google Calendar'),
  );

  $form['add_calendar']['rebuild'] = array(
    '#type' => 'button',
    '#value' => t('Rebuild Calender data'),
    '#limit_validation_errors' => array(),
    '#submit' => array(),
    '#ajax' => array(
      'callback' => 'gcalviews_overview_ajax_rebuild',
    ),
  );

  $form['overview'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Google Calendars'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['overview']['gcalviews_calendars_table'] = array(
    '#markup' => gcalviews_render_table(),
  );

  $form['overview']['pager'] = array(
    '#theme' => 'pager',
  );

  return $form;
}

/**
 * Implements hook_validate().
 */
function gcalviews_overview_validate($form, &$form_state) {

  $calendar_id = $form_state['values']['add_calendar']['calendar_id'];

  // Is $calendar_id a valid e-mail address?
  if (!valid_email_address($calendar_id)) {
    form_set_error('add_calendar][calendar_id', t('Please provide a valid Google Calendar ID'));
  }

  // Check if $calendar_id already exists in gcalviews_calendars table.
  $query = db_select('gcalviews_calendars', 'gcid')
   ->fields('gcid', array('calendarId'))
   ->condition('calendarId', $calendar_id, '=')
   ->execute();

  if ($query->rowCount() > 0) {
    form_set_error('add_calendar][calendar_id', t('The Google Calendar ID already exists'));
  }
}

/**
 * Implements hook_submit().
 */
function gcalviews_overview_submit($form, &$form_state) {

  $calendar_id = $form_state['values']['add_calendar']['calendar_id'];
  $account_id = $form_state['values']['add_calendar']['account_id'];

  // Retrieve calendar.
  $response = gcal_calendar_get($calendar_id, $account_id);

  if ($response) {
    // Insert into gcalviews_calendars table.
    db_insert('gcalviews_calendars')
     ->fields(array(
       'calendarId' => $calendar_id,
       'accountId' => $account_id,
     ))
     ->execute();

    // Update gcalviews_events table.
    gcalviews_add_resources($calendar_id, $account_id);

    drupal_set_message(t('Google Calendar successfully added'));
  }
  else {
    drupal_set_error(t('Google Calendar could not be added'), 'error');
  }
}

/**
 * Rebuild resources and display confirmation message.
 */
function gcalviews_overview_ajax_rebuild() {

  gcalviews_rebuild_resources();

  // Ajax show message.
  $commands = array(
    ajax_command_replace('#messages', '<div id="messages">' . theme('status_messages') . '</div>'),
    ajax_command_alert('Google Calendars successfully rebuilt'),
  );

  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

/**
 * Render a table showing added Google Calendars.
 */
function gcalviews_render_table() {

  // Header definition.
  $header = array(
    array(
      'data' => t('Calendar ID'),
      'field' => 'gcid.calendarId',
      'sort' => 'ASC',
    ),
    array(
      'data' => t('Google Account'),
      'field' => 'gcid.accountId',
    ),
    array(
      'data' => t('Actions'),
    ),
  );

  // Select table.
  $query = db_select('gcalviews_calendars', 'gcid')->extend('PagerDefault');

  // Select fields.
  $results = $query
   ->fields('gcid', array('gcid', 'calendarId', 'accountId'))
   ->extend('TableSort')
   ->orderByHeader($header)
   ->limit(10)
   ->execute();

  // Populate table.
  $rows = array();

  foreach ($results as $entry) {
    $rows[] = array(
      array('data' => $entry->calendarId),
      array('data' => $entry->accountId),
      array('data' => l(t('Remove'), 'admin/config/services/gcalviews/remove/' . $entry->gcid)),
    );
  }

  // Render table.
  $output = theme('table',
    array(
      'header' => $header,
      'rows' => $rows,
      'sticky' => TRUE,
      'empty' => 'No Google Calendars added',
    )
  );

  return $output;
}

/**
 * Retrieve GAuth account names from gauth_accounts table.
 */
function gcalviews_generate_account_list() {

  $list = array();

  // Retrieve account names from gauth_accounts table.
  $query = db_select('gauth_accounts', 'gaid');
  $results = $query
   ->fields('gaid', array('name'))
   ->execute();

  foreach ($results as $result) {
    $list[$result->name] = $result->name;
  }
  return $list;
}
