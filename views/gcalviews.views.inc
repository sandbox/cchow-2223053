<?php

/**
 * @file
 * GCal integration with Views.
 */

/**
 * Implements hook_views_data().
 */
function gcalviews_views_data() {

  $data['gcalviews_calendars'] = array(
    'table' => array(
      'group' => t('Google Calendar'),
      'base' => array(
        'field' => 'gcid',
        'title' => t('Google Calendars'),
        'help' => t('Display Google Calendars'),
      ),
    ),

    // Primary key field.
    'gcid' => array(
      'title' => t('Internal Google Calendar Object ID'),
    ),

    // CalendarId.
    'calendarId' => array(
      'title' => t('Google Calendar ID'),
      'help' => t('Identifier of the calendar'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // AccountId.
    'accountId' => array(
      'title' => t('Google Calendar account ID'),
      'help' => t('OAuth account used to access the calendar'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
  );

  $data['gcalviews_events'] = array(
    'table' => array(
      'group' => t('Google Calendar'),
      'base' => array(
        'field' => 'grid',
        'title' => t('Google Calendar events'),
        'help' => t('Display events from a Google calendar'),
      ),
    ),

    // Primary key field.
    'grid' => array(
      'title' => t('Internal Google Calendar Object ID'),
    ),

    // CalendarId.
    'calendarId' => array(
      'title' => t('Google Calendar ID'),
      'help' => t('Identifier of the calendar'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Kind.
    'kind' => array(
      'title' => t('Type'),
      'help' => t('Type of resource'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // etag.
    'etag' => array(
      'title' => t('Entity tag'),
      'help' => t('The event entity tag'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

     // Id.
    'id' => array(
      'title' => t('ID'),
      'help' => t('Identifier of the event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Status.
    'status' => array(
      'title' => t('Status'),
      'help' => t('Status of the event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // HtmlLink.
    'htmlLink' => array(
      'title' => t('HTML link'),
      'help' => t('An absolute link to this event in the Google Calendar Web UI'),
      'field' => array(
        'handler' => 'views_handler_field_url',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Created.
    'created' => array(
      'title' => t('Created'),
      'help' => t('Creation time of the event'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // Updated.
    'updated' => array(
      'title' => t('Updated'),
      'help' => t('Last modification time of the event'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // Summary.
    'summary' => array(
      'title' => t('Summary'),
      'help' => t('Title of the event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Description.
    'description' => array(
      'title' => t('Description'),
      'help' => t('Description of the event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Location.
    'location' => array(
      'title' => t('Location'),
      'help' => t('Geographic location of the event as free-form text'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Location_wkt.
    'location_wkt' => array(
      'title' => t('Location as a WKT string'),
      'help' => t('Geocoded wkt string of event location (requires the geocoder module)'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // ColorId.
    'colorId' => array(
      'title' => t('Color Id'),
      'help' => t('The color of the event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Creator_id.
    'creator_id' => array(
      'title' => t("Creator's ID"),
      'help' => t("The creator's Profile ID"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Creator_email.
    'creator_email' => array(
      'title' => t("Creator's e-mail address"),
      'help' => t("The creator's e-mail address"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Creator_displayName.
    'creator_displayName' => array(
      'title' => t("Creator's name"),
      'help' => t("The creator's name"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Creator_self.
    'creator_self' => array(
      'title' => t("Does this event belong to the creator's calendar?"),
      'help' => t('Whether the creator corresponds to the calendar on which this copy of the event appears'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Organizer_id.
    'organizer_id' => array(
      'title' => t("Organizer's ID"),
      'help' => t("The organizer's Profile ID"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Organizer_email.
    'organizer_email' => array(
      'title' => t("Organizer's e-mail address"),
      'help' => t("The organizer's e-mail address"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Organizer_displayName.
    'organizer_displayName' => array(
      'title' => t("Organizer's name"),
      'help' => t("The organizer's name"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),

    // Organizer_self.
    'organizer_self' => array(
      'title' => t("Does this event belong to the organizer's calendar?"),
      'help' => t('Whether the organizer corresponds to the calendar on which this copy of the event appears'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Start_date.
    'start_date' => array(
      'title' => t('Start date'),
      'help' => t('The date, in the format "yyyy-mm-dd", if this is an all-day event. For a recurring event, this is the start time of the first instance'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // Start_dateTime.
    'start_dateTime' => array(
      'title' => t('Start date-time'),
      'help' => t('The time, as a combined date-time value. For a recurring event, this is the start time of the first instance'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // Start_timeZone.
    'start_timeZone' => array(
      'title' => t('Start time zone'),
      'help' => t('The name of the time zone in which the time is specified'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // End_date.
    'end_date' => array(
      'title' => t('End date'),
      'help' => t('The date, in the format "yyyy-mm-dd", if this is an all-day event. For a recurring event, this is the end time of the first instance'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // End_dateTime.
    'end_dateTime' => array(
      'title' => t('End date-time'),
      'help' => t('The time, as a combined date-time value. For a recurring event, this is the end time of the first instance'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // End_timeZone.
    'end_timeZone' => array(
      'title' => t('End time zone'),
      'help' => t('The name of the time zone in which the time is specified'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // EndTimeUnspecified.
    'endTimeUnspecified' => array(
      'title' => t('Is the end time unspecified?'),
      'help' => t('Whether the end time is actually unspecified'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // recurrence. TODO.
    'recurrence' => array(
      'title' => t('Recurrence'),
      'help' => t('List of RRULE, EXRULE, RDATE and EXDATE lines for a recurring event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // recurringEventId.
    'recurringEventId' => array(
      'title' => t('Recurring event ID'),
      'help' => t('For an instance of a recurring event, this is the event ID of the recurring event itself'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // OriginalStartTime_date.
    'originalStartTime_date' => array(
      'title' => t('Original start date'),
      'help' => t('The date, in the format "yyyy-mm-dd", if this is an all-day event. For an instance of a recurring event, this is the time at which this event would start according to the recurrence data in the recurring event identified by recurringEventId'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // OriginalStartTime_dateTime.
    'originalStartTime_dateTime' => array(
      'title' => t('Original start date-time'),
      'help' => t('The time, as a combined date-time value. For an instance of a recurring event, this is the time at which this event would start according to the recurrence data in the recurring event identified by recurringEventId'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
    ),

    // OriginalStartTime_timeZone.
    'originalStartTime_timeZone' => array(
      'title' => t('Original start time zone'),
      'help' => t('The name of the time zone in which the time is specified. For an instance of a recurring event, this is the time at which this event would start according to the recurrence data in the recurring event identified by recurringEventId'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Transparency.
    'transparency' => array(
      'title' => t('Transparency'),
      'help' => t('Whether the event blocks time on the calendar'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Visibility.
    'visibility' => array(
      'title' => t('Visibility'),
      'help' => t('Visibility of the event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // ICalUID.
    'iCalUID' => array(
      'title' => t('iCal unique ID'),
      'help' => t('Event ID in the iCalendar format'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Sequence.
    'sequence' => array(
      'title' => t('Sequence'),
      'help' => t('Sequence number as per iCalendar'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // AttendeesOmitted.
    'attendeesOmitted' => array(
      'title' => t('Attendees omitted?'),
      'help' => t("Whether attendees may have been omitted from the event's representation"),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),
  );

  $data['gcalviews_attendees'] = array(
    'table' => array(
      'group' => t('Google Calendar'),
      'base' => array(
        'field' => 'gaid',
        'title' => t('Google Calendar attendees'),
        'help' => t('Display attendees to an event from a Google calendar'),
      ),
    ),

    // Primary key field.
    'grid' => array(
      'title' => t('Internal Google Calendar Object ID'),
    ),

    // EventId.
    'eventId' => array(
      'title' => t('Event ID'),
      'help' => t('The ID of the related event'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Id.
    'id' => array(
      'title' => t('ID'),
      'help' => t("The attendee's Profile ID"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Email.
    'email' => array(
      'title' => t('E-mail'),
      'help' => t("The attendee's email address"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // DisplayName.
    'displayName' => array(
      'title' => t('Attendee names'),
      'help' => t("The attendee's name"),
      'field' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_string',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Organizer.
    'organizer' => array(
      'title' => t('Is the attendee the organizer?'),
      'help' => t('Whether the attendee is the organizer of the event'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Self.
    'self' => array(
      'title' => t('Does the attendee own this event?'),
      'help' => t('Whether this entry represents the calendar on which this copy of the event appears'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Resource.
    'resource' => array(
      'title' => t('Is the attendee a resource?'),
      'help' => t('Whether the attendee is a resource'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Optional.
    'optional' => array(
      'title' => t('Is the attendee optional?'),
      'help' => t('Whether this is an optional attendee'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // ResponseStatus.
    'responseStatus' => array(
      'title' => t('Response status'),
      'help' => t("The attendee's response status"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // Comment.
    'comment' => array(
      'title' => t('Comment'),
      'help' => t("The attendee's response comment"),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument',
      ),
    ),

    // AdditionalGuests.
    'additionalGuests' => array(
      'title' => t('Additional guests'),
      'help' => t("The attendee's response comment"),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
  );

  $data['gcalviews_events']['table']['join']['gcalviews_attendees'] = array(
    'type' => 'INNER',
    'left_field' => 'eventId',
    'field' => 'id',
  );

  return $data;
}
